import './App.css';
import PaymentForm from './components/payment-form/payment-form.component';

function App() {
  return (
    <div >
      <PaymentForm />
    </div>
  );
}

export default App;
