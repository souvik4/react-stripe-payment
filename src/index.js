import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Elements} from '@stripe/react-stripe-js'

import {stripePromise} from './utils/stripe/stripe.utils'

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>
// );

ReactDOM.render(
  <React.StrictMode>
    <Elements stripe={stripePromise}>
     <App />
    </Elements>
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
